﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof(BoxCollider))]

public class PlatformTrigger : MonoBehaviour
{
    #region FIELDS
    public UnityEvent OnCollisionEnterEvent = new UnityEvent();
    #endregion

    #region PROPERTIES 
    //CACHES
    private bool EnemyWasSpawned { get; set; } = false;
    #endregion

    #region FUNCTIONS

    protected void OnCollisionEnter(Collision collision)
    {
        if (!GameplayManager.Instance || !GameplayManager.Instance.InstancePlayer)
            return;

        if (collision.gameObject == GameplayManager.Instance.InstancePlayer)
            RunEvent();
    }

    private void RunEvent()
    {
        if (!EnemyWasSpawned)
        {
            OnCollisionEnterEvent.Invoke();
            EnemyWasSpawned = true;
        }
    }
    #endregion

    #region ENUM_CLASSES

    #endregion
}