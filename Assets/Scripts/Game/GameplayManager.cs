﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

public class GameplayManager : MonoBehaviour
{
    #region FIELDS
    public UnityEvent OnLostGame = new UnityEvent();
    public UnityEvent OnWinGame = new UnityEvent();
    public UnityEvent OnAddKeys = new UnityEvent();

    private static GameplayManager instance = null;

    [SerializeField]
    private int keysToWinGame = 5;
    [SerializeField]
    private GameObject player = null;
    [SerializeField]
    private Transform playerSpawnPoint = null;
    [SerializeField]
    private GameObject winPlatform = null;
    [SerializeField]
    private float waitForRestart = 3f;
    #endregion

    #region PROPERTIES
    public int KeysToWinGame {
        get => keysToWinGame;
        private set => keysToWinGame = value;
    }
    private GameObject Player {
        get => player;
        set => player = value;
    }
    private Transform PlayerSpawnPoint {
        get => playerSpawnPoint;
        set => playerSpawnPoint = value;
    }
    public GameObject WinPlatform {
        get => winPlatform;
        set => winPlatform = value;
    }
    public float WaitForRestart {
        get => waitForRestart;
        set => waitForRestart = value;
    }

    //CACHES
    public static GameplayManager Instance
    {
        get => instance;
        private set => instance = value;
    }
    public int CollectedKeys { get; private set; }
    public GameObject InstancePlayer { get; private set; }

    private Coroutine Coroutine { get; set; }
    #endregion

    #region FUNCTIONS
    protected void Awake()
    {
        if (Instance == null)
            Instance = this;
        else if (Instance != this)
            Destroy(gameObject);

        if(Player && PlayerSpawnPoint)
        {
            InstancePlayer = Instantiate<GameObject>(Player, PlayerSpawnPoint.position, Quaternion.identity);
        }
    }
     
    protected void Start()
    {
        if (InstancePlayer)
        {
            HealthComponent healthComponent = InstancePlayer.GetComponent<HealthComponent>();

            if (healthComponent)
                healthComponent.OnDeadEvent.AddListener(LostGame);
        }

        Time.timeScale = 1f;

        Cursor.visible = false;
    }

    protected void OnDestroy()
    {
        if(InstancePlayer)
        {
            HealthComponent healthComponent = InstancePlayer.GetComponent<HealthComponent>();

            if (healthComponent)
                healthComponent.OnDeadEvent.RemoveListener(LostGame);
        }

        if(Coroutine != null)
            StopCoroutine(Coroutine);
    }

    public void AddKey(Key key)
    {
        if (key)
        {
            CollectedKeys++;
            OnAddKeys.Invoke();

            if(IsAllKeySCollected())
                OnCollectedAllKeys(); 
        }
    }

    public void OnCollectedAllKeys()
    {
        WinPlatform.SetActive(true);
    }

    public void WinGame()
    {
        if (InputManager.Instance)
        {
            InputManager.Instance.DeactiveGameplayInputs();
        }

        OnWinGame.Invoke();

        Coroutine = StartCoroutine(WaitAndRestartGame(WaitForRestart));

        Time.timeScale = 0f;
    }
    public void LostGame()
    {
        if (InputManager.Instance)
        {
            InputManager.Instance.DeactiveGameplayInputs();
        }

        OnLostGame.Invoke();

        Coroutine = StartCoroutine(WaitAndRestartGame(WaitForRestart));

        Time.timeScale = 0f;
    }
    private bool IsAllKeySCollected()
    {
        return CollectedKeys == KeysToWinGame;
    }

    private IEnumerator WaitAndRestartGame(float waitTime)
    {
        yield return new WaitForSecondsRealtime(waitTime);

        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    #endregion

    #region ENUM_CLASSES

    #endregion
}