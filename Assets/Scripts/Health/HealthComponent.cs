﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class HealthComponent : MonoBehaviour
{
    #region FIELDS
    public UnityEvent OnDeadEvent = new UnityEvent();
    public UnityEvent OnChangeHealthEvent = new UnityEvent();

    [SerializeField]
    private float health = 5f;
    #endregion

    #region PROPERTIES
    public float Health {
        get => health;
        private set => health = value;
    }    
    #endregion

    #region FUNCTIONS
    public void TakeDamage(float damage)
    {
        Health -= damage;

        OnChangeHealthEvent.Invoke();

        if (IsDead())
            OnDeadEvent.Invoke();
    }

    public bool IsDead()
    {
        return Health <= 0f;
    }
    #endregion

    #region ENUM_CLASSES

    #endregion
}
