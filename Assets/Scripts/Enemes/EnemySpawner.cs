﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    #region FIELDS
    [SerializeField]
    private GameObject enemy = null;

    [SerializeField]
    private Transform enemySpawnTranform = null;
    #endregion

    #region PROPERTIES
    private GameObject Enemy
    {
        get => enemy;
        set => enemy = value;
    }
    private Transform EnemySpawnTranform
    {
        get => enemySpawnTranform;
        set => enemySpawnTranform = value;
    }
    #endregion

    #region FUNCTIONS
   
    public void SpawnEnemy()
    {
        if (Enemy && EnemySpawnTranform)
        {
            Instantiate(Enemy, EnemySpawnTranform.position, Quaternion.identity);
        }
    }
	
	#endregion
	
	#region ENUM_CLASSES
	
	#endregion
}
