﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(NavMeshAgent))]

public class EnemyController : MonoBehaviour
{
    #region FIELDS
    [SerializeField]
    private GameObject key = null;
    [SerializeField]
    private float rateOfAttack = 1f;
    [SerializeField]
    private float maxDistanceToAttack = 2f;
    [SerializeField]
    private float damage = 1f;
    #endregion

    #region PROPERTIES
    private GameObject Key {
        get => key;
        set => key = value;
    }
    private float RateOfAttack {
        get => rateOfAttack;
        set => rateOfAttack = value;
    }
    private float MaxDistanceToAttack {
        get => maxDistanceToAttack;
        set => maxDistanceToAttack = value;
    }
    private float Damage {
        get => damage;
        set => damage = value;
    }

    //CACHES
    private NavMeshAgent Agent { get; set; }
    private GameObject Goal { get; set; }
    private HealthComponent HealthComponent { get; set; }
    private bool CanAttack { get; set; }
    Coroutine Coroutine { get; set; }

    #endregion

    #region FUNCTIONS
    protected void Start()
    {
        if (GameplayManager.Instance)
        {
            Goal = GameplayManager.Instance.InstancePlayer;
        }
        Agent = GetComponent<NavMeshAgent>();
        HealthComponent = GetComponent<HealthComponent>();

        if (HealthComponent)
            HealthComponent.OnDeadEvent.AddListener(OnDead);

        CanAttack = true;
    }

    protected void OnDestroy()
    {
        if (HealthComponent)
            HealthComponent.OnDeadEvent.RemoveListener(OnDead);

        if(Coroutine != null)
            StopCoroutine(Coroutine);
    }

    private void Update()
    {
        if (!Goal || !Agent)
            return;

        Agent.destination = Goal.transform.position;

        TryAttack();
    }
    private void OnDead()
    {
        SpawnKey();

        Destroy(gameObject);
    }

    private void SpawnKey()
    {
        if (Key)                                       
            Instantiate(Key, transform.position, Quaternion.identity);
    }

    private void TryAttack()
    {
        if (CanAttack && IsCloseEnough())
        {
            CanAttack = false;

            HealthComponent healthComponent = Goal.GetComponent<HealthComponent>();

            if (healthComponent)
                healthComponent.TakeDamage(damage);

            Coroutine = StartCoroutine(WaitForRateOFAttack(RateOfAttack));
        }
    }

    private bool IsCloseEnough()
    {
        return Vector3.Distance(transform.position, Goal.transform.position) <= MaxDistanceToAttack;
    }

    private IEnumerator WaitForRateOFAttack(float rateOfAttack)
    {
        yield return new WaitForSeconds(RateOfAttack);

        CanAttack = true;
    }
    #endregion

    #region ENUM_CLASSES

    #endregion
}