﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(CapsuleCollider))]
public class MovementPlayerComponent : MonoBehaviour
{
    #region FIELDS
    [SerializeField]
    private float moveSpeed = 10f;
    [SerializeField]
    private float rotateSpeed = 1f;
    [SerializeField]
    private float jumpForce = 10f;
    #endregion

    #region PROPERTIES
    private float MoveSpeed
    {
        get => moveSpeed;
        set => moveSpeed = value;
    }
    private float RotateSpeed
    {
        get => rotateSpeed;
        set => rotateSpeed = value;
    }
    private float JumpForce
    {
        get => jumpForce;
        set => jumpForce = value;
    }
      
    //CACHED
    float DistanceToGround { get; set; }
    private Rigidbody Rigidbody { get; set; }
    private CapsuleCollider CapsuleCollider { get; set; }
    #endregion

    #region FUNCTIONS
    protected void Awake()
    {
        Rigidbody = GetComponent<Rigidbody>();
        CapsuleCollider = GetComponent<CapsuleCollider>();

        if(CapsuleCollider)
            DistanceToGround = CapsuleCollider.bounds.extents.y;
    }

    protected void Start()
    {
        if (!InputManager.Instance || !InputManager.Instance.MovementInputController)
        {
            return;
        }

        InputManager.Instance.MovementInputController.OnHorizontalInputEvent.AddListener(HorizontalMove);
        InputManager.Instance.MovementInputController.OnVerticalInputEvent.AddListener(VerticalMove);
        InputManager.Instance.MovementInputController.OnRotateInputEvent.AddListener(Rotate);
        InputManager.Instance.MovementInputController.OnJumpEvent.AddListener(Jump);
    }
    protected void OnDestroy()
    {
        if (!InputManager.Instance || !InputManager.Instance.MovementInputController)
            return;

        InputManager.Instance.MovementInputController.OnHorizontalInputEvent.RemoveListener(HorizontalMove);
        InputManager.Instance.MovementInputController.OnVerticalInputEvent.RemoveListener(VerticalMove);
        InputManager.Instance.MovementInputController.OnRotateInputEvent.RemoveListener(Rotate);
        InputManager.Instance.MovementInputController.OnJumpEvent.RemoveListener(Jump);
    }

    private void Jump()
    {
        if(IsGrounded())
            Rigidbody.AddForce(transform.up * JumpForce, ForceMode.Force);
    }

    private void HorizontalMove(float direction)
    {
        transform.position -= transform.forward * MoveSpeed * Time.deltaTime * direction;
    }

    private void VerticalMove(float direction)
    {
        transform.position += transform.right * MoveSpeed * Time.deltaTime * direction;
    }

    private void Rotate(float direction)
    {
        transform.Rotate(Vector3.up * RotateSpeed * direction);
    }

    private bool IsGrounded()
    {
        return Physics.Raycast(transform.position, -Vector3.up, DistanceToGround + 0.1f);
    }
    #endregion

    #region ENUM_CLASSES

    #endregion
}
