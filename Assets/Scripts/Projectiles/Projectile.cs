﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(SphereCollider))]

public class Projectile : MonoBehaviour
{
    #region FIELDS
    [SerializeField]
    float lifeTime = 5f;
    [SerializeField]
    float damage = 1f;
    #endregion

    #region PROPERTIES
    private float LifeTime {
        get => lifeTime;
        set => lifeTime = value;
    }
    private float Damage {
        get => damage;
        set => damage = value;
    }

    Rigidbody Rigidbody { get; set; }
    SphereCollider SphereCollider { get; set; }
    #endregion

    #region FUNCTIONS
    protected void Awake()
    {
        Rigidbody = GetComponent<Rigidbody>();
        SphereCollider = GetComponent<SphereCollider>();
    }

    protected void Start()
    {
        Destroy(gameObject, LifeTime);
    }
    void OnCollisionEnter(Collision collision)
    {
        HealthComponent healthComponent = collision.gameObject.GetComponent<HealthComponent>();

        if (healthComponent)
            healthComponent.TakeDamage(Damage);

        Destroy(gameObject);
    }
    #endregion

    #region ENUM_CLASSES     
    #endregion
}