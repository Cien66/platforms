﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Events;
using UnityEngine;


public class ShotInputController : MonoBehaviour
{
    #region FIELDS      
    public UnityEvent OnShotEvent = new UnityEvent();
    public UnityEvent OnOnLeaveTrapEvent = new UnityEvent();
    #endregion

    #region PROPERTIES
    #endregion

    #region FUNCTIONS      
    protected void Update()
    {
        if (Input.GetAxis("Fire1") != 0f && enabled)
        {
            OnShotEvent.Invoke();           
        }

        if (Input.GetAxis("Fire2") != 0f && enabled)
        {
            OnOnLeaveTrapEvent.Invoke();
        }
    }
    #endregion

    #region ENUM_CLASSES

    #endregion
}