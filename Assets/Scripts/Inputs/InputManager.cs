﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
                     
public class InputManager : MonoBehaviour
{
    #region FIELDS
    [SerializeField]
    private MovementInputController movementInputController;
    [SerializeField]
    private ShotInputController shotInputController;
    
    private static InputManager instance = null;
    #endregion

    #region PROPERTIES                  
    public MovementInputController MovementInputController {
        get => movementInputController;
        private set => movementInputController = value;
    }
    public ShotInputController ShotInputController {
        get => shotInputController;
        private set => shotInputController = value;
    }
    //CACHES
    public static InputManager Instance {
        get => instance;
        private set => instance = value;
    }    
    #endregion

    #region FUNCTIONS
    protected void Awake()
    {
        if (Instance == null)
            Instance = this;
        else if (Instance != this)
            Destroy(gameObject);
    }

    protected void Start()
    {
        ActiveGameplayInputs();
    }

    public void ActiveGameplayInputs()
    {
        if (MovementInputController)
            MovementInputController.enabled = true;

        if (ShotInputController)
            ShotInputController.enabled = true;
    }

    public void DeactiveGameplayInputs()
    {
        if (MovementInputController)
            MovementInputController.enabled = false;

        if (ShotInputController)
            ShotInputController.enabled = false;
    }
    #endregion

    #region ENUM_CLASSES

    #endregion
}
