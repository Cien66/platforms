﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Events;
using UnityEngine;


public class MovementInputController : MonoBehaviour
{

    #region FIELDS
    public UnityEvent<float> OnHorizontalInputEvent = new UnityFloatEvent();
    public UnityEvent<float> OnVerticalInputEvent = new UnityFloatEvent();
    public UnityEvent<float> OnRotateInputEvent = new UnityFloatEvent();
    public UnityEvent OnJumpEvent = new UnityEvent();
    #endregion

    #region PROPERTIES
    #endregion

    #region FUNCTIONS
    protected void Update()
    {
        if (enabled)
        {
            UpdateMove();
            UpdateRotation();
            UpdateJump();
        }
    }

    private void UpdateJump()
    {
        if (Input.GetAxis("Jump") != 0f)
            OnJumpEvent.Invoke();
    }

    private void UpdateMove()
    {
        float horizontalDirection = Input.GetAxis("Horizontal");
        float verticalDirection = Input.GetAxis("Vertical");

        if (horizontalDirection != 0f)
            OnHorizontalInputEvent.Invoke(horizontalDirection);

        if (verticalDirection != 0f)
            OnVerticalInputEvent.Invoke(verticalDirection);
    }

    private void UpdateRotation()
    {
        float rotateDirection = Input.GetAxis("Mouse X");

        if (rotateDirection != 0)
            OnRotateInputEvent.Invoke(rotateDirection);
    }
    #endregion

    #region ENUM_CLASSES
    [System.Serializable]
    public class UnityFloatEvent : UnityEvent<float>
    {}
    #endregion
}