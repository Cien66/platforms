﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(BoxCollider))]
public class Key : MonoBehaviour
{
    #region FIELDS
    #endregion

    #region PROPERTIES                                       
    #endregion

    #region FUNCTIONS
    private void OnTriggerEnter(Collider other)
    {
        if(GameplayManager.Instance && GameplayManager.Instance.InstancePlayer == other.gameObject)
        {
            GameplayManager.Instance.AddKey(this);
            Destroy(gameObject);
        }
    }

    #endregion

    #region ENUM_CLASSES

    #endregion
}
