﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIController : MonoBehaviour
{
    #region FIELDS                                   
    [SerializeField]
    private Text resultGameText;
    [SerializeField]
    private Text healthText;
    [SerializeField]
    private Text keysText;
    [SerializeField]
    private string winMessage = "U win";
    [SerializeField]
    private string lostMessage = "U were murdered!";
    [SerializeField]
    private string healthMessage = "Health: ";
    [SerializeField]
    private string keysFormat = "{0}/{1}";
    #endregion

    #region PROPERTIES
    private Text ResultGameText {
        get => resultGameText;
        set => resultGameText = value;
    }
    private Text HealthText {
        get => healthText;
        set => healthText = value;
    }
    private string WinMessage {
        get => winMessage;
        set => winMessage = value;
    }
    private string LostMessage {
        get => lostMessage;
        set => lostMessage = value;
    }
    public Text KeysText {
        get => keysText;
        set => keysText = value;
    }
    public string KeysFormat {
        get => keysFormat;
        set => keysFormat = value;
    }

    //CACHES
    private HealthComponent HealthComponent { get; set; }

    #endregion

    #region FUNCTIONS
    protected void Start()
    {
        if (!GameplayManager.Instance)
            return;

        GameplayManager.Instance.OnLostGame.AddListener(OnLostGame);
        GameplayManager.Instance.OnWinGame.AddListener(OnWinGame);
        GameplayManager.Instance.OnAddKeys.AddListener(OnAddKey);

        if(GameplayManager.Instance.InstancePlayer)
        {
            HealthComponent healthComponent = GameplayManager.Instance.InstancePlayer.GetComponent<HealthComponent>();
            if (healthComponent)
            {
                healthComponent.OnChangeHealthEvent.AddListener(OnChangeHealth);
                HealthComponent = healthComponent;
            }
        }

        OnChangeHealth();
        OnAddKey();
    }

    protected void OnDestroy()
    {
        if (!GameplayManager.Instance)
            return;

        GameplayManager.Instance.OnLostGame.RemoveListener(OnLostGame);
        GameplayManager.Instance.OnWinGame.RemoveListener(OnWinGame);
        GameplayManager.Instance.OnAddKeys.RemoveListener(OnAddKey);

        if (GameplayManager.Instance.InstancePlayer)
        {
            if (HealthComponent)
            {
                HealthComponent.OnChangeHealthEvent.RemoveListener(OnChangeHealth);
                HealthComponent = null;
            }
        }
    }

    private void OnChangeHealth()
    {
        if(HealthComponent && HealthText)
        {
            HealthText.text = healthMessage + HealthComponent.Health;  
        }
    }
    private void OnWinGame()
    {
        if (ResultGameText)
        {
            ResultGameText.text = WinMessage;
            ResultGameText.gameObject.SetActive(true);
        }
    }

    private void OnLostGame()
    {
        if (ResultGameText)
        {
            ResultGameText.text = LostMessage;
            ResultGameText.gameObject.SetActive(true);
        }
    }

    private void OnAddKey()
    {
        if(KeysText && GameplayManager.Instance)
        {
            int collected = GameplayManager.Instance.CollectedKeys;
            int all = GameplayManager.Instance.KeysToWinGame;
            KeysText.text = string.Format(KeysFormat, collected, all);
        }
    }
	#endregion
	
	#region ENUM_CLASSES
	
	#endregion
}
