﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShotComponent : MonoBehaviour
{
    #region FIELDS
    [SerializeField]
    private float rateOfFire = 0.2f;
    [SerializeField]
    private GameObject bullet = null;
    [SerializeField]
    private Transform muzzleTransform = null;
    [SerializeField]
    private float bulletSpeed = 20f;
    #endregion

    #region PROPERTIES
    private float RateOfFire {
        get => rateOfFire;
        set => rateOfFire = value;
    }
    private GameObject Bullet {
        get => bullet;
        set => bullet = value;
    }
    public Transform MuzzleTransform {
        get => muzzleTransform;
        set => muzzleTransform = value;
    }
    public float BulletSpeed {
        get => bulletSpeed;
        set => bulletSpeed = value;
    }

    //CACHED
    private bool CanShot { get; set; }

    private Coroutine Coroutine { get; set; }
    #endregion


    #region FUNCTIONS
    protected void Start()
    {
        CanShot = true;

        if (InputManager.Instance && InputManager.Instance.ShotInputController)
            InputManager.Instance.ShotInputController.OnShotEvent.AddListener(Shot);
    }

    protected void OnDestroy()
    {
        if(Coroutine != null)
            StopCoroutine(Coroutine);

        if (InputManager.Instance)
            InputManager.Instance.ShotInputController.OnShotEvent.RemoveListener(Shot);
    }

    public void Shot()
    {
        if (CanShot && Bullet)
        {
            GameObject bullet = Instantiate<GameObject>(Bullet, MuzzleTransform.position, Quaternion.identity);

            Rigidbody riginbody = bullet ? bullet.GetComponent<Rigidbody>() : null;

            if (riginbody)
            {
                riginbody.AddForce(MuzzleTransform.forward * BulletSpeed, ForceMode.Impulse);

                CanShot = false;

                Coroutine = StartCoroutine(WaitForRateOfFire(RateOfFire));
            }
        }
    }

    private IEnumerator WaitForRateOfFire(float rateOfFire)
    {
        yield return new WaitForSeconds(rateOfFire);

        CanShot = true;
    }
    #endregion

    #region ENUM_CLASSES

    #endregion
}