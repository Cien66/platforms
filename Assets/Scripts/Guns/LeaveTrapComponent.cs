﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LeaveTrapComponent : MonoBehaviour
{
    #region FIELDS
    [SerializeField]
    private GameObject trap = null;
    [SerializeField]
    private float rateOfFire = 0.2f;
    #endregion

    #region PROPERTIES
    public GameObject Trap {
        get => trap;
        set => trap = value;
    }
    private float RateOfFire {
        get => rateOfFire;
        set => rateOfFire = value;
    }

    //CACHES
    bool CanLeaveTrap { get; set; }

    private Coroutine Coroutine { get; set; }
    #endregion

    #region FUNCTIONS
    protected void Start()
    {
        if(InputManager.Instance && InputManager.Instance.ShotInputController)
        {
            InputManager.Instance.ShotInputController.OnOnLeaveTrapEvent.AddListener(LeaveTrap);
        }

        CanLeaveTrap = true;
    }

    protected void OnDestroy()
    {
        if (InputManager.Instance && InputManager.Instance.ShotInputController)
        {
            InputManager.Instance.ShotInputController.OnOnLeaveTrapEvent.RemoveListener(LeaveTrap);
        }

        if (Coroutine != null)
            StopCoroutine(Coroutine);
    }

    private void LeaveTrap()
    {
        if (Trap && CanLeaveTrap)
        {
            Instantiate(Trap, transform.position, Quaternion.identity);

            CanLeaveTrap = false;

            Coroutine = StartCoroutine(WaitForRateOfFire(RateOfFire));
        }
    }

    private IEnumerator WaitForRateOfFire(float rateOfFire)
    {
        yield return new WaitForSeconds(rateOfFire);

        CanLeaveTrap = true;
    }
    #endregion

    #region ENUM_CLASSES

    #endregion
}
