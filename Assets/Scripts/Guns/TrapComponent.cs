﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrapComponent : MonoBehaviour
{
    #region FIELDS
    [SerializeField]
    float lifeTime = 5f;
    [SerializeField]
    float damage = 1f;
    #endregion

    #region PROPERTIES
    private float LifeTime {
        get => lifeTime;
        set => lifeTime = value;
    }
    private float Damage {
        get => damage;
        set => damage = value;
    }

    Rigidbody Rigidbody { get; set; }
    SphereCollider SphereCollider { get; set; }
    #endregion

    #region FUNCTIONS
    
    protected void Start()
    {
        Destroy(gameObject, LifeTime);
    }
    private void OnTriggerEnter(Collider other)
    {
        if (GameplayManager.Instance && GameplayManager.Instance.InstancePlayer)
        {
            if (GameplayManager.Instance.InstancePlayer == other.gameObject) 
                return;
        }

        HealthComponent healthComponent = other.gameObject.GetComponent<HealthComponent>();

        if (healthComponent)
            healthComponent.TakeDamage(Damage);

        Debug.Log(other.gameObject.name);

        Destroy(gameObject);
    }
    #endregion

    #region ENUM_CLASSES

    #endregion
}
